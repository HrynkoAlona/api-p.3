
import { UsersController } from "/Users/alyonahrynko/Documents/Git/BSA/api-p.3/tests/api/lib/controllers/users.controller";
import { AuthController } from "/Users/alyonahrynko/Documents/Git/BSA/api-p.3/tests/api/lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "/Users/alyonahrynko/Documents/Git/BSA/api-p.3/tests/helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();
const chai = require('chai');
chai.use(require('chai-json-schema'));

xdescribe("Token usage", () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("test_spachok88@gmail.com", "W16W92mM");

        accessToken = response.body.token.accessToken.token;
        // console.log(accessToken);
    });

    let userId: number;

    it(`Should deleting user by id`, async () => {
        let response = await users.deleteUser (3780); 

        checkStatusCode (response, 200);
        checkResponseTime (response, 1000);
    });

    it(`Should not found user details by id`, async () => {
        let response = await users.getUserById(3780);  
    
        checkStatusCode (response, 404);
        checkResponseTime (response, 1000);
    });

})