import {
    checkResponseTime,
    checkStatusCode, 
} from '/Users/alyonahrynko/Documents/Git/BSA/api-p.3/tests/helpers/functionsForChecking.helper';
import { AuthController } from '/Users/alyonahrynko/Documents/Git/BSA/api-p.3/tests/api/lib/controllers/auth.controller';
const auth = new AuthController();

describe('Use test data set for login', () => {
    let invalidCredentialsDataSet = [
        { email: 'test_spachok88@gmail.com', password: '12345' },
        { email: 'test_spachok88@gmail.com', password: 'W16W12312' },
        { email: 'test_spachok88@gmail.com', password: 'W16W92mM ' },
        { email: 'test_spachok88@gmail.com', password: 'test_spachok88@gmail.com' },
        { email: 'test_spachok88gmail.com', password: 'W16W92m' },
        { email: 'test_spachok88@gmaicom', password: '   ' },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not login using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await auth.login(credentials.email, credentials.password);

            checkStatusCode(response, 401); 
            checkResponseTime(response, 3000);
        });
    });
});

