import { expect } from "chai";
import { UsersController } from "/Users/alyonahrynko/Documents/Git/BSA/api-p.3/tests/api/lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "/Users/alyonahrynko/Documents/Git/BSA/api-p.3/tests/helpers/functionsForChecking.helper";

const users = new UsersController();
const schemas = require('/Users/alyonahrynko/Documents/Git/BSA/api-p.3/tests/api/specs/data/schemas_testData.json');
const chai = require('chai');
chai.use(require('chai-json-schema'));

xdescribe(`Users controller`, () => {
    let userId: number;

it(`Should return user details when getting user details by id`, async () => {
    let response = await users.getUserById(3780);  

    checkStatusCode (response, 200);
    checkResponseTime (response, 1000);
    expect(response.body.id, `Response body ID should be 3780`).to.be.equal(3780);

    // console.log(response.body);
});

})

