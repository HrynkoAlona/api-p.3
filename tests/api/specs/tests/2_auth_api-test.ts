
import { UsersController } from "/Users/alyonahrynko/Documents/Git/BSA/api-p.3/tests/api/lib/controllers/users.controller";
import { AuthController } from "/Users/alyonahrynko/Documents/Git/BSA/api-p.3/tests/api/lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "/Users/alyonahrynko/Documents/Git/BSA/api-p.3/tests/helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();

xdescribe("Token usage", () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("test_spachok88@gmail.com", "W16W92mM");

        accessToken = response.body.token.accessToken.token;
        // console.log(accessToken);
    });

    it(`Usage is here`, async () => {
        let userData: object = {
            id: 3780,
            avatar: "string",
            email: "test_spachok88@gmail.com",
            userName: "Alona",
        };

        let response = await users.updateUser(userData, accessToken);
        checkStatusCode (response, 204);
    });
});

