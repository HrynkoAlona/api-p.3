
import { RegisterController } from "/Users/alyonahrynko/Documents/Git/BSA/api-p.3/tests/api/lib/controllers/register.controller";
import { checkStatusCode, checkResponseTime } from "/Users/alyonahrynko/Documents/Git/BSA/api-p.3/tests/helpers/functionsForChecking.helper";

const register = new RegisterController();
const chai = require('chai');
chai.use(require('chai-json-schema'));

xdescribe(`Register controller`, () => {

    it(`Create new user`, async () => {
        let userData: object = {
            id: 0,
            avatar: "string",
            email: "test_spachok88@gmail.com",
            userName: "Alona",
            password: "W16W92mM"
        };

        let response = await register.registerUser (userData);
        checkStatusCode (response, 201);
        console.log(response.body);
    });
})