import { ApiRequest } from "/Users/alyonahrynko/Documents/Git/BSA/api-p.3/tests/api/lib/request";

export class AuthController {
    async login(emailValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Auth/login`)
            .body({
                email: emailValue,
                password: passwordValue,
            })
            .send();
        return response;
    }
}
