import { ApiRequest } from "/Users/alyonahrynko/Documents/Git/BSA/api-p.3/tests/api/lib/request";

export class RegisterController {

    async registerUser (userData: object) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/register`)
            .body(userData)
            .send();
        return response;
    }
}